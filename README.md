Project Name: CISCO Field Notice Check
Created By: Michael Gottwald
Last Edited: 8/14/2020 10:50 AM

Files necessary for running
-- HTML.py
-- config_AIR.txt
-- config_ASA.txt
-- config_ASR9001.txt
-- config_C2960.txt
-- config_C3560.txt
-- config_CISCO39.txt
-- config_CTS.txt
-- config_GLC.txt
-- config_ISR.txt
-- config_N9K.txt
-- config_UCS.txt

System Requirements
---- Python 3.8
---- chromedriver_linux64.zip
---- Version 84.0.4147.125 (Official Build) (64-bit) "Version of Chrome this is meant for"

Python Libraries Required:
--- -- time
--- -- smtplib
--- -- mysql.connector
--- -- re
--- -- selenium import webdriver
--- -- selenium.webdriver.common.by import By
--- -- selenium.webdriver.support.ui import WebDriverWait
--- -- selenium.webdriver.support import expected_conditions as EC

Notes from developer:
- Make sure you have the chromedriver zip file that matches your operating system. If you are using Windows, there is a windows chromdriver at this link <https://chromedriver.chromium.org/downloads>
- If there is an update to the Cisco field notice website, the script will crash.
- Starting at line 49, the email login configuration begins.
- Edits made to the email formatting can be done in the message_string variable starting at line 20
- Edits made to the mailing list can be done in toaadr and msg starting at line 56
- Recommended to run at 10:00 AM 12:00 PM and 1:00 PM or before items are shipped for the day