import time 
import smtplib
import mysql.connector
import re
from selenium import webdriver 
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# def flattenNestedList(nestedList):
    ## Converts a nested list to a flat list. (Makes it one long list instead of compounded ones)
    # flatList = []
    # for elem in nestedList:
        # if isinstance(elem, list):
            # flatList.extend(flattenNestedList(elem))
        # else:
            # flatList.append(elem)
    # return flatList

def send_items_added_email(new_quotes, hyperlink, issue): #Once given a list of possibly affected part numbers, this will send out a neat table with the relevant information.
    message_string = '''
        <table style="border-spacing: 0px;" width="600px" align="center">
            <tr style="background:#00467c; color:#ffffff;" >
                <td style="padding:10px;" colspan="4" align="center"><h1>Field Notice Alert</h1><h2>''' + issue + '''</td>   
            </tr>
        <tr style="background:#333333; color:#FFFFFF;" align="center">
            <td style="padding:10px;"><strong>Serial Number</strong></td>
            <td style="padding:10px;"><strong>Field Notice ID</strong></td>
            <td style="padding:10px;"><strong>Part Number</strong></td>
            <td style="padding:10px;"><strong>Date Recieved</strong></td>
        </tr>
        <tr align="center">'''      #The table style is formatted above
    count = 0
    items = 0
    for x in new_quotes:    #Takes the list of possibly effected part numbers and drops them into the table, making sure that every 4 entries is it's own row
            message_string = message_string + '<td>' + str(x[0]) + '</td><td>' + str(x[1]) + '</td><td>' + str(x[2]) + '</td><td>' + str(x[3]) + '</td></tr><tr align="center">'

    message_string =  message_string + '''
                <tr style="background:#00467c; color:#ffffff;" align="center">
                <td style="padding:10px;" colspan="4" ><strong>If you would like more information about the specifics of the possible defect, <a href="''' + hyperlink + '''">visit this link.
            </tr>
        </table>
        ''' #Adds the relevant hyperlink to the part's field notice information
    if len(new_quotes) >1:
        subject_line = "CISCO FIELD NOTICE ALERT: " + str(len(new_quotes)) + " Possibly Affected Items in Warehouse: " + issue
    else:
        subject_line = "CISCO FIELD NOTICE ALERT: " + str(len(new_quotes)) + " Possibly Affected Item in Warehouse: " + issue

###3#####3###3##3####   The enclosed information is the setup and connection information for the google account. 
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()

    username = 'stats@p3systemsinc.com'
    password = '235middleROAD!'
    fromaddr = 'stats@p3systemsinc.com'
    toaddr = ['m.gottwald@p3systemsinc.com','dgeorge@p3systemsinc.com','operations@p3online.com','warehouse@p3systemsinc.com']
    msg = '\r\n'.join(['From: stats@p3systemsinc.com', 'To: m.gottwald@p3systemsinc.com,dgeorge@p3systemsinc.com, operations@p3online.com, warehouse@p3systemsinc.com', 
                       'MIME-Version: 1.0','Content-type: text/html',
                       'Subject: ' + subject_line + '',
                       '',
                       '' + message_string + ''])
    server.login(username, password)
    server.sendmail(fromaddr, toaddr, msg)
    server.quit()
###3####3###3###3####

###############
# affecedList takes in an array of 4 variables: serial_num,fn_id,part_num,date
# After accessing the website, there are multiple times where the program must delay to input keys into the webdriver. These are indicated by the time.sleep(#)
# Once the array has been input into the page, regex parses the HTML into only a list of affected items.
# The output is an array of affected part numbers only.
###############
def affectedList(data):    #Checks the CISCO field notice website for the list of parts that haven't been checked yet from the SQL warehouse database
    options = webdriver.ChromeOptions()
    options.add_argument("--start-maximized")
    driver = webdriver.Chrome(chrome_options=options)
    driver.get ("https://snvui.cisco.com/snv/FN"+ data[0][1])
    driver.find_element_by_id('userInput').send_keys('engineering@p3systemsinc.com')
    driver.find_element_by_id('login-button').click()
    time.sleep(5)
    driver.find_element_by_id ('password').send_keys('235middleROAD!')
    driver.find_element_by_id('kc-login').click()
    time.sleep(4)
    for a in data:
        serial = a[0]
        driver.find_element_by_name('serialNumber').send_keys(serial)
    time.sleep(1)
    try:
        element = WebDriverWait(driver, 30).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'Submit')]")))
        element.click()
        
        time.sleep(3)
        serial_div = driver.find_element_by_xpath('//div[@class="serialNoData"]')
        serial_rows = serial_div.find_elements_by_tag_name('tr')
        affected = []
        inc = 0
        for row in serial_rows:
            cells = row.get_attribute('innerHTML')
            a = re.findall('(?<=">).+(?=<)',cells)
            if a[1] == 'Affected':
                temp_list = (a[0],data[inc][1],data[inc][2],data[inc][3])
                affected.append(temp_list)
            inc = inc+1
        driver.quit()
        return(affected)
    except:
        driver.quit()
        print("Something went wrong. Nothing came back")

def open_mysql_connection(): # This is called in write_to_mysql and is used to connect to the warehouse database
    test_db = mysql.connector.connect(
        host="192.168.100.180",
        user="testlab",
        passwd="speakFRIEND9(9",
        database="p3systems"
    )
    return test_db

#################################
# All of the below functions are of the same format, and could be combined into 1 single function. They are not combined since the efficiency of the program remains the same
# First, the function accesses the SQL database, and opens the individual config file to know which address to start searching from
# All items in the warehouse DB that come back are passed into the affectedList function to be evaluated by the Field Notice Checker
# The error checking of the list_ variable is simple and just keeps the script from crashing.
#################################

def write_to_mysql_ASA():
    print('ASA')
    current_database = open_mysql_connection()
    h = open("config_ASA.txt",'r+')
    text = h.read()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%ASA%' AND item_id_pk > '" + str(text) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '64228'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_ASA.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_ISR():
    print('ISR')
    current_database = open_mysql_connection()
    h = open("config_ISR.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%ISR4%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '64253'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_ISR.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_N9K():
    print('N9K')
    current_database = open_mysql_connection()
    h = open("config_N9K.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%N9K%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '64251'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_N9K.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_AIR(): 
    print('AIR')
    current_database = open_mysql_connection()
    h = open("config_AIR.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%AIR-%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '70143'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_AIR.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_GLC(): 
    print('GLC')
    current_database = open_mysql_connection()
    h = open("config_GLC.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%GLC-SX%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '70120'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_GLC.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_UCS(): 
    print('UCS')
    current_database = open_mysql_connection()
    h = open("config_UCS.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%UCS%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '63499'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_UCS.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_CTS(): 
    print('CTS')
    current_database = open_mysql_connection()
    h = open("config_CTS.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%CTS%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '64217'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_CTS.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_ASR9001(): 
    print('ASR9001')
    current_database = open_mysql_connection()
    h = open("config_ASR9001.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%ASR-9001%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '63646'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_ASR9001.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_C2960(): 
    print('C2960')
    current_database = open_mysql_connection()
    h = open("config_C2960.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%C2960%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '63340'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_C2960.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_C3560(): 
    print('C3560')
    current_database = open_mysql_connection()
    h = open("config_C3560.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%C3560%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '63251'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_C3560.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
        
def write_to_mysql_CISCO39(): 
    print('CISCO39')
    current_database = open_mysql_connection()
    h = open("config_CISCO39.txt",'r+')
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%CISCO39%' AND item_id_pk > '" + str(h.read()) + "'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        date = row[15]
        fn_id = '63723'
        a = serial_num + ","
        temp = (str(a),fn_id, part_num,str(date))
        list_.append(temp)
    if list_:
        list_[-1] = (serial_num,fn_id,part_num,str(date))
        output = affectedList(list_)
        f = open("config_CISCO39.txt",'w')
        f.write(str(id_))
        current_database.commit()
        mycursor.close()
        return output
    else:
        try:  
            f = open("config_C3560.txt",'w')
            f.write(str(id_))
            current_database.commit()
            mycursor.close()
        except:
            f = open("config_C3560.txt",'w')
            f.write(str(h.read()))
            current_database.commit()
            mycursor.close()
#List of fn that are currently being checked: ASA/64228, ISR/64253, N9K/64251, AIR/70143, GLC/70120, UCS/63499, CTS/64217, ASR9001/63646, C2960/63340, C3560/63251, Cisco39/63723

#The funciton calls below include a URL with the Field Notices specific issues, plus a quick description of the issue. 
#The if function is true simply runs when items are returned that are on the field notice list

ASA = write_to_mysql_ASA()
if ASA:
    send_items_added_email(ASA, "https://www.cisco.com/c/en/us/support/docs/field-notices/642/fn64228.html", "Clock Signal Component Failure")

ISR = write_to_mysql_ISR()
if ISR:
    send_items_added_email(ISR, "https://www.cisco.com/c/en/us/support/docs/field-notices/642/fn64253.html?dtid=osscdc000283", "Clock Signal Component Failure")

N9K = write_to_mysql_N9K()
if N9K: 
    send_items_added_email(N9K, "https://www.cisco.com/c/en/us/support/docs/field-notices/642/fn64251.html?dtid=osscdc000283", "Clock Signal Component Failure")

AIR = write_to_mysql_AIR()
if AIR:
    send_items_added_email(AIR, "https://www.cisco.com/c/en/us/support/docs/field-notices/701/fn70143.html?dtid=osscdc000283", "Wireless Access Points Might Be Unable to Connect to the Wired Network")

GLC = write_to_mysql_GLC()
if GLC:
    send_items_added_email(GLC, "https://www.cisco.com/c/en/us/support/docs/field-notices/701/fn70120.html?dtid=osscdc000283", "Potential PCB Dimensional Offset")

UCS = write_to_mysql_UCS()
if UCS:
    send_items_added_email(UCS, "https://www.cisco.com/c/en/us/support/docs/field-notices/634/fn63499.html?dtid=osscdc000283", "Disk Drive Contamination Causes Premature Failure")

CTS = write_to_mysql_CTS()
if CTS:
    send_items_added_email(CTS, "https://www.cisco.com/c/en/us/support/docs/field-notices/642/fn64217.html?dtid=osscdc000283", "Faulty Camera Cable")

ASR9001 = write_to_mysql_ASR9001()
if ASR9001:
    send_items_added_email(ASR9001, "https://www.cisco.com/c/en/us/support/docs/field-notices/636/fn63646.html?dtid=osscdc000283", "MPAs Might Fail to Power Up")

C2960 = write_to_mysql_C2960()
if C2960:
    send_items_added_email(C2960, "https://www.cisco.com/c/en/us/support/docs/field-notices/633/fn63340.html?dtid=osscdc000283", "Power Supply Defect")

C3560 = write_to_mysql_C3560()
if C3560:
    send_items_added_email(C3560, "https://www.cisco.com/c/en/us/support/docs/field-notices/632/fn63251.html?dtid=osscdc000283", "Series Switches Have REdundant Power Supply covers of Incorrect Size")

CISCO39 = write_to_mysql_CISCO39()
if CISCO39:
    send_items_added_email(CISCO39, "https://www.cisco.com/c/en/us/support/docs/field-notices/637/fn63723.html", "Fans Might Fail Due to Capacitor Issue")